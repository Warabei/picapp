package my.picapp.recycler;

import my.picapp.entities.Picture;

/**
 * Created by warabei14 on 11/17/15.
 */
public interface OnPictureClickListener {
    void onClick(Picture picture);
}
