package my.picapp.recycler;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import my.picapp.R;
import my.picapp.entities.Picture;

import java.util.List;

/**
 * Created by warabei14 on 11/17/15.
 */
public class PictureAdapter extends RecyclerView.Adapter<PictureItemHolder> {

    private final LayoutInflater mLayoutInflater;
    private final List<Picture> mPictures;
    private final Picasso mPicasso;
    private final OnPictureClickListener mClickListener;

    public PictureAdapter(List<Picture> pictures, Context context, Picasso picasso, OnPictureClickListener onClickListener) {
        mPictures = pictures;
        mPicasso = picasso;
        mClickListener = onClickListener;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public PictureItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PictureItemHolder(getItemView(parent), mPicasso, mClickListener);
    }

    private View getItemView(ViewGroup parent) {
        return mLayoutInflater.inflate(R.layout.item_picture, parent, false);
    }

    @Override
    public void onBindViewHolder(PictureItemHolder holder, int position) {
        holder.bind(mPictures.get(position));
    }

    @Override
    public int getItemCount() {
        return mPictures.size();
    }

    public List<Picture> getItems(){
        return mPictures;
    }

    public void addItems(List<Picture> pictures) {
        mPictures.addAll(pictures);
    }

    public void clearPictures() {
        mPictures.clear();
    }
}
