package my.picapp.recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import my.picapp.R;
import my.picapp.entities.Image;
import my.picapp.entities.Picture;

/**
 * Created by warabei14 on 11/17/15.
 */
public class PictureItemHolder extends RecyclerView.ViewHolder{

    private final TextView mAuthor;
    private final TextView mTitle;
    private final ImageView mPicture;
    private final Picasso mPicasso;
    private final OnPictureClickListener mClickListener;

    public PictureItemHolder(View itemView, Picasso mPicasso, final OnPictureClickListener clickListener) {
        super(itemView);
        this.mPicasso = mPicasso;
        this.mClickListener = clickListener;
        mAuthor = (TextView) itemView.findViewById(R.id.author);
        mTitle = (TextView) itemView.findViewById(R.id.title_);
        mPicture = (ImageView) itemView.findViewById(R.id.picture);
    }

    public void bind(final Picture picture) {
        mPicasso.load(picture.imageUrls.get(Image.SMALL_SIZE).url).into(mPicture);
        mTitle.setText(picture.title);
        mAuthor.setText(picture.user.nickName);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickListener.onClick(picture);
            }
        });
    }

}
