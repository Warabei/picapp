package my.picapp.utils;

import android.content.Context;
import my.picapp.R;
import my.picapp.entities.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warabei14 on 11/18/15.
 */
public class CategoriesUtils {
    public static List<Category> getActualCategories(Context context) {
        String[] catArray = context.getResources().getStringArray(R.array.categories);
        List <Category> categories = new ArrayList<Category>(catArray.length + 1);
        categories.add(new Category(Category.FAKE, context.getString(R.string.no_category)));
        for (String cat :  catArray){
            String [] split = cat.split("\\|");
            categories.add(new Category(Integer.valueOf(split[0]), split[1]));
        }
        return categories;
    }
}
