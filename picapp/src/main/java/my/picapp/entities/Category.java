package my.picapp.entities;

/**
 * Created by warabei14 on 11/17/15.
 */
public class Category {

    public static final int FAKE = -1;
    private int id;
    private String name;

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
