package my.picapp.entities;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by warabei14 on 11/17/15.
 */
public class Picture implements Parcelable{

    @SerializedName("description")
    public String title;

    @SerializedName("images")
    public List<Image> imageUrls;

    @SerializedName("user")
    public User user;

    public static final Parcelable.Creator<Picture> CREATOR = new Parcelable.Creator<Picture>() {

        @Override
        public Picture createFromParcel(Parcel source) {
            return new Picture(source);
        }

        @Override
        public Picture[] newArray(int size) {
            return new Picture[size];
        }
    };

    private Picture (Parcel source) {
        title = source.readString();
        source.readList(imageUrls, Image.class.getClassLoader());
        user = source.readParcelable(getClass().getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeList(imageUrls);
        dest.writeParcelable(user, flags);
    }
}
