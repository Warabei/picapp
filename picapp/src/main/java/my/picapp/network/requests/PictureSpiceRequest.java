package my.picapp.network.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import my.picapp.entities.Picture;
import my.picapp.network.NetworkService;
import my.picapp.network.responses.PicturesResponse;

/**
 * Created by warabei14 on 11/18/15.
 */
public class PictureSpiceRequest extends RetrofitSpiceRequest<PicturesResponse, NetworkService> {

    private final int mPage;
    private final int mCategory;

    public PictureSpiceRequest(int page, int category) {
        super(PicturesResponse.class, NetworkService.class);
        mPage = page;
        mCategory = category;
    }

    @Override
    public PicturesResponse loadDataFromNetwork() throws Exception {
        return getService().requestPictures(mPage, mCategory);
    }
}
