package my.picapp.network;

import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;

/**
 * Created by warabei14 on 11/18/15.
 */
public class RetrofitSpiceService extends RetrofitGsonSpiceService {

    private static final String BASE_URL = "https://api.500px.com/";

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(NetworkService.class);
    }

    @Override
    protected String getServerUrl() {
        return BASE_URL;
    }
}
