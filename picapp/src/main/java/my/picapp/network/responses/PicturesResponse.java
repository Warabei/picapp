package my.picapp.network.responses;

import com.google.gson.annotations.SerializedName;
import my.picapp.entities.Picture;

import java.util.List;

/**
 * Created by warabei14 on 11/17/15.
 */
public class PicturesResponse {

    @SerializedName("photos")
    public List <Picture> pictures;
}
