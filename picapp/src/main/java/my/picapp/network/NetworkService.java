package my.picapp.network;

import my.picapp.network.responses.PicturesResponse;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by warabei14 on 11/17/15.
 */
public interface NetworkService {

    @GET("/v1/photos?feature=fresh_today&image_size[]=3&image_size[]=600" +
            "&consumer_key=4dfVrqF8dHahB6r1n3KYdkaWA49JlyDhllau7VjX")
    PicturesResponse requestPictures(@Query("page") int page, @Query("only") int category);
}
