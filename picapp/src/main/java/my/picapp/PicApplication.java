package my.picapp;

import android.app.Application;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;

/**
 * Created by warabei14 on 11/17/15.
 */
public class PicApplication extends Application {

    private Picasso mPicasso;

    @Override
    public void onCreate() {
        super.onCreate();
        mPicasso = new Picasso.Builder(this).memoryCache(new LruCache(16 * 1024 * 1024)).build();
    }

    public Picasso getPicasso() {
        return mPicasso;
    }
}
