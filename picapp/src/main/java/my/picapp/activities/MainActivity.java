package my.picapp.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import my.picapp.R;
import my.picapp.entities.Category;
import my.picapp.entities.Picture;
import my.picapp.fragments.GridFragment;
import my.picapp.fragments.dialogs.PictureDialogFragment;
import my.picapp.fragments.listeners.PictureFragmentListener;
import my.picapp.utils.CategoriesUtils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements PictureFragmentListener, AdapterView.OnItemSelectedListener {

    private static final String SPINNER_SELECTION = "spinner_selection";
    private Spinner mCategoriesSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        serveBundle(savedInstanceState);
    }

    private void init() {
        mCategoriesSpinner = (Spinner) findViewById(R.id.categories_spinner);
        SpinnerAdapter adapter = new ArrayAdapter<Category>(
                this, android.R.layout.simple_list_item_1, CategoriesUtils.getActualCategories(this));
        mCategoriesSpinner.setAdapter(adapter);
        mCategoriesSpinner.setOnItemSelectedListener(this);
    }

    private void serveBundle(Bundle savedInstanceState) {
        if (savedInstanceState == null){
            getFragmentManager().beginTransaction().replace(R.id.content_frame, new GridFragment(), "categories").commit();
        } else {
            mCategoriesSpinner.setSelection(savedInstanceState.getInt(SPINNER_SELECTION));
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SPINNER_SELECTION, mCategoriesSpinner.getSelectedItemPosition());
    }

    @Override
    public void showPicture(Picture picture) {
        PictureDialogFragment fragment = new PictureDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(PictureDialogFragment.PICTURE, picture);
        fragment.setArguments(bundle);
        fragment.show(getFragmentManager(), "full");
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            notifyGrid((Category) parent.getAdapter().getItem(position));
        }
    }

    private void notifyGrid(Category category) {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.content_frame);
        if (fragment instanceof GridFragment) {
            ((GridFragment)fragment).onCategoryChanged(category);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
