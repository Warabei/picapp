package my.picapp.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.squareup.picasso.Picasso;
import my.picapp.PicApplication;

/**
 * Created by warabei14 on 11/18/15.
 */
public class BaseActivity extends AppCompatActivity {

    private Picasso mPicasso;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mPicasso = ((PicApplication) getApplication()).getPicasso();
        super.onCreate(savedInstanceState);
    }

    protected Picasso getPicasso(){
        return mPicasso;
    }
}
