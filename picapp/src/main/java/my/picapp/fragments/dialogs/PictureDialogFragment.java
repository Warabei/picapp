package my.picapp.fragments.dialogs;

import android.app.Activity;
import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import my.picapp.R;
import my.picapp.entities.Image;
import my.picapp.entities.Picture;
import my.picapp.fragments.listeners.PictureFragmentListener;

/**
 * Created by warabei14 on 11/17/15.
 */
public class PictureDialogFragment extends BaseDialogFragment<PictureFragmentListener> {

    public static final String PICTURE = "PictureDialogFragment_PICTURE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Picture picture = getArguments().getParcelable(PICTURE);
        View view = inflater.inflate(R.layout.dialog_picture, container, false);
        ImageView pictureView = (ImageView) view.findViewById(R.id.picture);
        TextView author = (TextView) view.findViewById(R.id.author);
        TextView title = (TextView) view.findViewById(R.id.title_);
        getPicasso().load(picture.imageUrls.get(Image.BIG_SIZE).url).into(pictureView);
        title.setText(picture.title);
        author.setText(picture.user.nickName);
        return view;
    }


}
