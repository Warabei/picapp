package my.picapp.fragments.dialogs;

import android.app.Activity;
import android.app.DialogFragment;
import com.squareup.picasso.Picasso;
import my.picapp.PicApplication;
import my.picapp.fragments.listeners.FragmentListener;

/**
 * Created by warabei14 on 11/18/15.
 */
public class BaseDialogFragment <T extends FragmentListener> extends DialogFragment {

    private Picasso mPicasso;
    private T mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mPicasso = ((PicApplication) activity.getApplication()).getPicasso();
        mListener = (T) activity;
    }

    protected Picasso getPicasso(){
        return mPicasso;
    }

    protected T getListener() {
        return mListener;
    }
}
