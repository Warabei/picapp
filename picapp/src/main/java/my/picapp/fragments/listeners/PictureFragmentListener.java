package my.picapp.fragments.listeners;


import my.picapp.entities.Picture;

/**
 * Created by warabei14 on 11/17/15.
 */
public interface PictureFragmentListener extends FragmentListener {

    void showPicture(Picture picture);

}
