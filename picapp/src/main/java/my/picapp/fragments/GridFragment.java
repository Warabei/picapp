package my.picapp.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import my.picapp.R;
import my.picapp.entities.Category;
import my.picapp.entities.Picture;
import my.picapp.fragments.listeners.PictureFragmentListener;
import my.picapp.network.requests.PictureSpiceRequest;
import my.picapp.network.RetrofitSpiceService;
import my.picapp.network.responses.PicturesResponse;
import my.picapp.recycler.OnPictureClickListener;
import my.picapp.recycler.PictureAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warabei14 on 11/17/15.
 */
public class GridFragment extends BaseFragment<PictureFragmentListener> implements OnPictureClickListener, RequestListener<PicturesResponse> {

    private static final String PICTURES = "pictures";
    private static final String CURRENT_PAGE = "currentPage";
    private static final String CATEGORY = "category";
    private static final String RETRY_LAST_REQUEST = "retryLastRequest";

    private RecyclerView mPicturesRecyclerView;
    private int mSpanCount;
    private int mCurrentPage;
    private int mCategoryId = -1;
    private boolean mIsLoading;
    private SpiceManager mSpiceManager;
    private PictureAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        init(view);
        serveBundle(savedInstanceState);
        setScrollListener();
    }

    @Override
    public void onResume() {
        mSpiceManager.start(getActivity());
        super.onResume();
    }

    @Override
    public void onPause() {
        mSpiceManager.shouldStop();
        super.onPause();
    }

    private void init(View view) {
        mPicturesRecyclerView = (RecyclerView) view;
        mSpanCount = calcSpanCount();
        mPicturesRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), mSpanCount));
        mSpiceManager = new SpiceManager(RetrofitSpiceService.class);
    }

    private void serveBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mCategoryId = savedInstanceState.getInt(CATEGORY);
            mCurrentPage = savedInstanceState.getInt(CURRENT_PAGE);
            List<Picture> pictures = savedInstanceState.getParcelableArrayList(PICTURES);
            if (pictures != null) {
                setPictures(pictures);
            }
            if (savedInstanceState.getBoolean(RETRY_LAST_REQUEST)) {//check if request lost over recreating activity
                requestPictures(mCategoryId, mCurrentPage);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(CURRENT_PAGE, mCurrentPage);
        outState.putInt(CATEGORY, mCategoryId);
        outState.putBoolean(RETRY_LAST_REQUEST, mIsLoading);
        if (mAdapter != null) {
            List<Picture> pictures = mAdapter.getItems();
            outState.putParcelableArrayList(PICTURES, (ArrayList<? extends Parcelable>) pictures);
        }
    }

    private void setScrollListener() {
        mPicturesRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = layoutManager.getItemCount();

                int visibleItemCount = layoutManager.findLastVisibleItemPosition();
                if (!mIsLoading && visibleItemCount + mSpanCount > totalItemCount) {
                    mCurrentPage++;
                    mIsLoading = true;
                    requestPictures(mCategoryId, mCurrentPage);
                }
            }
        });
    }

    private int calcSpanCount() {
        int width = getScreenWidth();
        int itemWidth = getItemWidth();
        return Math.max(1, width / itemWidth);
    }

    private int getItemWidth() {
        return getResources().getDimensionPixelSize(R.dimen.picture_dem);
    }

    private int getScreenWidth() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return metrics.widthPixels;
    }

    public void onCategoryChanged(Category category) {
        if (category.getId() != mCategoryId) {
            tryRemovePictures();
            mCurrentPage = 1;
            mCategoryId = category.getId();
            requestPictures(mCategoryId, mCurrentPage);
        }
    }

    private void requestPictures(int id, int page) {
        mIsLoading = true;
        PictureSpiceRequest request = new PictureSpiceRequest(page, id);
        String cache = id + "cache" + page;
        mSpiceManager.execute(request, cache, DurationInMillis.ONE_MINUTE, this);
    }

    public void setPictures(List<Picture> pictures) {
        mAdapter = new PictureAdapter(pictures, getActivity(), getPicasso(), GridFragment.this);
        mPicturesRecyclerView.setAdapter(mAdapter);
    }

    private void addPictures(List<Picture> pictures) {
        int itemCount = mAdapter.getItemCount();
        mAdapter.addItems(pictures);
        mAdapter.notifyItemRangeChanged(itemCount, pictures.size());
    }

    private void tryRemovePictures() {
        if (mAdapter != null) {
            mAdapter.clearPictures();
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(Picture picture) {
        getListener().showPicture(picture);
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        mIsLoading = false;
    }

    @Override
    public void onRequestSuccess(PicturesResponse picturesResponse) {
        mIsLoading = false;
        if (mAdapter == null) {
            setPictures(picturesResponse.pictures);
        } else {
            addPictures(picturesResponse.pictures);
        }
    }
}
