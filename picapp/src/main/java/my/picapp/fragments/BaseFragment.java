package my.picapp.fragments;

import android.app.Activity;
import android.app.Fragment;
import com.squareup.picasso.Picasso;
import my.picapp.PicApplication;
import my.picapp.fragments.listeners.FragmentListener;

/**
 * Created by warabei14 on 11/18/15.
 */
public class BaseFragment <T extends FragmentListener> extends Fragment {

    private Picasso mPicasso;
    private T mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mPicasso = ((PicApplication) activity.getApplication()).getPicasso();
        mListener = (T) activity;
    }

    protected Picasso getPicasso(){
        return mPicasso;
    }

    protected T getListener() {
        return mListener;
    }
}
